# Nodebots Day 2019

[![HeatSync Labs](HeatsyncLabs_logo.png)](https://www.heatsynclabs.org)



![chirpers](chirpers.jpg)


## Our bots for today

* Custom Chassis
* Arduino Leonardo
* Continuous Motion Servo
* Batteries
* wheels
* Wires


[firmware](https://github.com/monteslu/webusb-serial/tree/master/example/sketch/StandardFirmataWebUSB)


## Connecting to the bot with Chirpers


[https://chirpers.com/browser](https://chirpers.com/browser)


## Controlling the bot

Switch to IOT Remote Buttons view in menu.

![controls](controls.jpg)


# MIDI controller

* Leonardo - Micrcontroller
* buttons
* potentiometers
* wires

![leonardo](leonardo.jpg)

![midi_wiring](midi_wiring.png)





[MIDI firmware](midi_firmware)